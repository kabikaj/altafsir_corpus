#!/usr/bin/env python3
#
#    altafsir_tagging.py
#
# apply POS taggind to altafsir corpus using Camel tools
#
# usage:
#   $ python altafsir_tagging.py > ..data/processed/altafsir_processed.txt
#
##########################################################################

import os
import sys
import ujson as json

from camel_tools.utils.dediac import dediac_ar
from camel_tools.utils.normalize import normalize_alef_maksura_ar
from camel_tools.utils.normalize import normalize_alef_ar
from camel_tools.utils.normalize import normalize_teh_marbuta_ar
from camel_tools.tokenizers.word import simple_word_tokenize
from camel_tools.morphology.database import MorphologyDB
from camel_tools.morphology.analyzer import Analyzer
from camel_tools.disambig.mle import MLEDisambiguator
#from camel_tools.tokenizers.morphological import MorphologicalTokenizer

DATA_PATH = '../../data/all/final/'

def preprocess(s):
    s = dediac_ar(s)
    s = normalize_alef_maksura_ar(s)
    s = normalize_alef_ar(s)
    return normalize_teh_marbuta_ar(s)

if __name__ == '__main__':

    db = MorphologyDB.builtin_db()
    analyzer = Analyzer(db)
    mle = MLEDisambiguator.pretrained()
    #tokenizer = MorphologicalTokenizer(mle, scheme='atbseg')
    #tokenizer = MorphologicalTokenizer(mle, scheme='atbtok')
    #tokenizer = MorphologicalTokenizer(mle, scheme='bwtok')
    
    #FIXME do proper sort to filenames
    fobjs = (o for o in os.scandir(DATA_PATH) if o.is_file() and o.name.endswith('.json'))
    
    results = []
    for fo in fobjs:

        print(f'Processig file {fo.name}', file=sys.stderr) #TRACE

        with open(fo.path) as fp:

            text = json.load(fp)['text']
            text_tok = simple_word_tokenize(preprocess(text))
            text_disamb = mle.disambiguate(text_tok)
            #text_diac = ' '.join(d.analyses[0].analysis['diac'] for d in text_disamb)
            #print(text_diac)

            res = ('\t'.join((d.word,
                              d.analyses[0].analysis['diac'],
                              d.analyses[0].analysis['pos'],
                              d.analyses[0].analysis['lex'])) for d in text_disamb)

            results.append('\n'.join(res))

    for res in results:
        print(res, file=sys.stdout)

# example first analysis in text_disamb
# DisambiguatedWord(word='وقوله',
#                   analyses=[ScoredAnalysis(score=1.0,
#                                            analysis={'diac': 'وَقَوْلِهِ',
#                                                      'lex': 'قَوْل_1',
#                                                      'bw': 'وَ/CONJ+قَوْل/NOUN+ِ/CASE_DEF_GEN+هُ/POSS_PRON_3MS',
#                                                      'gloss': 'and+statement;remark+its;his',
#                                                      'pos': 'noun',
#                                                      'prc3': '0',
#                                                      'prc2': 'wa_conj',
#                                                      'prc1': '0',
#                                                      'prc0': '0',
#                                                      'per': 'na',
#                                                      'asp': 'na',
#                                                      'vox': 'na',
#                                                      'mod': 'na',
#                                                      'stt': 'c',
#                                                      'cas': 'g',
#                                                      'enc0': '3ms_poss',
#                                                      'rat': 'i',
#                                                      'source': 'lex',
#                                                      'form_gen': 'm',
#                                                      'form_num': 's',
#                                                      'pattern': 'وَ1َوْ3ِهِ',
#                                                      'root': 'ق.#.ل',
#                                                      'catib6': 'PRT+NOM+NOM',
#                                                      'ud': 'CONJ+NOUN+PRON',
#                                                      'd1seg': 'وَ+_قَوْلِهِ',
#                                                      'd1tok': 'وَ+_قَوْلِهِ',
#                                                      'atbseg': 'وَ+_قَوْلِ_+هِ',
#                                                      'd3seg': 'وَ+_قَوْلِ_+هِ',
#                                                      'd2seg': 'وَ+_قَوْلِهِ',
#                                                      'd2tok': 'وَ+_قَوْلِهِ',
#                                                      'atbtok': 'وَ+_قَوْلِ_+هُ',
#                                                      'd3tok': 'وَ+_قَوْلِ_+هُ',
#                                                      'bwtok': 'وَ+_قَوْل_+ِ_+هِ',
#                                                      'pos_lex_logprob': -3.478864,
#                                                      'caphi': 'w_a_q_a_w_l_i_h_i',
#                                                      'pos_logprob': -0.4344233,
#                                                      'gen': 'm',
#                                                      'lex_logprob': -3.478864,
#                                                      'num': 's',
#                                                      'stem': 'قَوْل',
#                                                      'stemgloss': 'statement;remark',
#                                                      'stemcat': 'N'})])
