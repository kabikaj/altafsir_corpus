

import os

files = (f for f in os.scandir('../../data/all/ori') if f.is_file())


for f in files:
    if f.name.count('-') == 6:
        srcfpath = f.path
        dstfpath = f.path.rsplit('-', 1)[0]+'.html'
        os.rename(srcfpath, dstfpath)
