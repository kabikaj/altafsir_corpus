#!/usr/bin/python3.4

#
#    util.py
#
# utility functions
#
#####################

from sys import stderr,exit

#
# data: index mappings for generating the query combinations in crawler.py
#

#                 Sura : No. of Ayahs
suraAyahTable = {
                   1   : 7   ,
                   2   : 286 ,
                   3   : 200 ,
                   4   : 176 ,
                   5   : 120 ,
                   6   : 165 ,
                   7   : 206 ,
                   8   : 75  ,
                   9   : 129 ,
                   10  : 109 ,
                   11  : 123 ,
                   12  : 111 ,
                   13  : 43  ,
                   14  : 52  ,
                   15  : 99  ,
                   16  : 128 ,
                   17  : 111 ,
                   18  : 110 ,
                   19  : 98  ,
                   20  : 135 ,
                   21  : 112 ,
                   22  : 78  ,
                   23  : 118 ,
                   24  : 64  ,
                   25  : 77  ,
                   26  : 227 ,
                   27  : 93  ,
                   28  : 88  ,
                   29  : 69  ,
                   30  : 60  ,
                   31  : 34  ,
                   32  : 30  ,
                   33  : 73  ,
                   34  : 54  ,
                   35  : 45  ,
                   36  : 83  ,
                   37  : 182 ,
                   38  : 88  ,
                   39  : 75  ,
                   40  : 85  ,
                   41  : 54  ,
                   42  : 53  ,
                   43  : 89  ,
                   44  : 59  ,
                   45  : 37  ,
                   46  : 35  ,
                   47  : 38  ,
                   48  : 29  ,
                   49  : 18  ,
                   50  : 45  ,
                   51  : 60  ,
                   52  : 49  ,
                   53  : 62  ,
                   54  : 55  ,
                   55  : 78  ,
                   56  : 96  ,
                   57  : 29  ,
                   58  : 22  ,
                   59  : 24  ,
                   60  : 13  ,
                   61  : 14  ,
                   62  : 11  ,
                   63  : 11  ,
                   64  : 18  ,
                   65  : 12  ,
                   66  : 12  ,
                   67  : 30  ,
                   68  : 52  ,
                   69  : 52  ,
                   70  : 44  ,
                   71  : 28  ,
                   72  : 28  ,
                   73  : 20  ,
                   74  : 56  ,
                   75  : 40  ,
                   76  : 31  ,
                   77  : 50  ,
                   78  : 40  ,
                   79  : 46  ,
                   80  : 42  ,
                   81  : 29  ,
                   82  : 19  ,
                   83  : 36  ,
                   84  : 25  ,
                   85  : 22  ,
                   86  : 17  ,
                   87  : 19  ,
                   88  : 26  ,
                   89  : 30  ,
                   90  : 20  ,
                   91  : 15  ,
                   92  : 21  ,
                   93  : 11  ,
                   94  : 8   ,
                   95  : 8   ,
                   96  : 19  ,
                   97  : 5   ,
                   98  : 8   ,
                   99  : 8   ,
                   100 : 11  ,
                   101 : 11  ,
                   102 : 8   ,
                   103 : 3   ,
                   104 : 9   ,
                   105 : 5   ,
                   106 : 4   ,
                   107 : 7   ,
                   108 : 3   ,
                   109 : 6   ,
                   110 : 3   ,
                   111 : 5   ,
                   112 : 4   ,
                   113 : 5   ,
                   114 : 6 
}


#                     Madhab ID : List of corresponding Tafsir IDs
madhabTafsirTable = {
                       1  : [1,2,4,5,6,7,8,9]  ,
                       2  : [10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,28,67,75,78,79,88,91,94,96,99,100,101,102,103,104,105] ,
                       3  : [29,30,31,32,33,36,37,92,95,97] ,
                       4  : [3,38,39,41,42,40,56] ,
                       5  : [44,45,47,89] ,
                       6  : [48,49,51] ,
                       7  : [52,53,54,55,57,76] ,
                       8  : [60,90] ,
                       9  : [50,65,68,71,83,84,85] ,
                       10 : [66,98]
}


#
# functions
#

def loadReferences(fileObj):
    """Read input file and extracts sura:ayah references.
    Args:
        fileObj (_io.TextIOWrapper): file object to parse.
    Return:
        Set of 2-string tuples, each containg number of sura and aya.
    """
    # read reference file and remove empty lines and comments, keep index to line numbers
    lines=((i,l.partition('#')[0].strip()) for i,l in enumerate(fileObj,1) if not l.startswith('#') and l.strip())
    
    # split sura : aya
    refs=set(map(lambda x: (x[0],tuple(x[1].split(':'))), lines))
    
    # check there are no errors in references file and exit if a problem is encountered
    for i,ref in refs:
        nsura = ref[0]
        naya = ref[1]

        if len(ref)!=2:
            print('Error in reference_file in line %s: "%s" must have exactly two elements (sura:ayah)' % (i,ref), file=stderr)
            exit(1)

        if not nsura.isdigit() or not naya.isdigit():
            print('Error in reference_file in line %s: "(%s:%s)" must be digits' % (i,nsura,naya), file=stderr)
            exit(1)

        if int(nsura) not in suraAyahTable:
            print('Error in reference_file in line %s: number of sura "%s" not in range (1-%d)' % (i,nsura,max(suraAyahTable.keys())), file=stderr)
            exit(1)

        if suraAyahTable[int(nsura)]<int(naya):
            print('Error in reference_file in line %s: sura %s has at must %d number of ayas' % (i,nsura,suraAyahTable[int(nsura)]), file=stderr)
            exit(1)
    
    # remove line numbres from refs
    return set(r[1] for r in refs)
  

def generateSuraAya(table=suraAyahTable):
    "Generates all sura,aya indexes in quran"
    for isura,naya in table.items():
        for jaya in range(1,naya+1):
            yield isura,jaya