Altafsir extractor
==================

*Corpus of quranic exegesis*


Scripts and relevant info for downloading all quranic commentaries from [altafsir.com][altafsir] concerning specific *sura* and *ayah* references. List of files:

[altafsir]: http://www.altafsir.com

* `crawler.py` - Script that downloads all html files corresponding to *sura:ayah* references given a reference file to given output directory.

* `scraper.py` - Script that extracts relevant information from original html files and dumps it into json files according to each different query.

* `merger.py` - Script that removes files with duplicate contain and fix inconsistencies in quranic indexes.

* `offsetbuilder.py` - Script that joins all text within each file and creates offsets for annotation.

* `selector.py` - Script that selects the relevant files for COBHUNI project.

* `util.py` - Utility functions and tables containing sura:ayah and madhab:tafsir correspondences.

* **references.txt** - *Sura:ayah* references.

* **madhab-names.txt** - Index and madhab name correspondences.

* **tafsir-names.txt** - Index and tafsir name, author and date correspondences.

For more info see the [wiki][]

[WIKI]: http://www.wiki-cobhuni.uni-hamburg.de/index.php/Al-tafsir_processing

