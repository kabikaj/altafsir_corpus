#!/usr/bin/python3.4
#
#     crawler.py
#
# web crawler for altafasir.com
# 
# usage:
#   $ python -u crawler.py ../../data/all/ori --debug |& tee 140420.log
#   $ nohup python crawler.py --ref_file references.txt ../../data/all/ori/ &
#
# dependencies:
#     util.py
#                                                                             # 
# Notes:                                                                     #   احفظ الرمز يا كبيكج
# * web to crawl, www.altafsir.com, does not include robots.txt             #
# * Web to check user agent: http://whatsmyuseragent.com/
# * web to view xml: http://codebeautify.org/
# * beautiful soup guide: http://omz-software.com/pythonista/docs/ios/beautifulsoup_guide.html
#
####################################################################################

import os
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.firefox.options import Options

#from pyvirtualdisplay import Display

from util import madhabTafsirTable, loadReferences, generateSuraAya
from sys import stdin, stdout, stderr, exit
from os.path import join as joinpath
from urllib.request import urlopen,Request
from urllib import parse
from bs4 import BeautifulSoup
from argparse import ArgumentParser,FileType
from time import time,sleep
from random import uniform

#os.environ['MOZ_HEADLESS'] = '1' #NOTE geckodriver

parser = ArgumentParser(description='download all commentaries of sura:aya pairs from altafsir.com')
parser.add_argument('--ref_file', type=FileType('r'), help='download only list of sura:aya pairs in this file')
parser.add_argument('directory_name', action='store', help='directory to store all html files')
parser.add_argument('--debug', action='store_true', help='print each query data to stderr')

args = parser.parse_args()

# ────────────────────────────────────────────────────
# constants and configuration
# ────────────────────────────────────────────────────

SLEEP_MIN = 0.986
SLEEP_MAX = 1.22

#URL = 'http://altafsir.com/Tafasir.asp'
URL = 'https://www.altafsir.com/Tafasir.asp'

VARS = { 'tMadhNo'     : '0',     # 1-10
         'tTafsirNo'   : '0',     # 1-105 (depends on tMadhNo chosen)
         'tSoraNo'     : '0',     # 1-114
         'tAyahNo'     : '0',     # 1-286 (depends on the tSoraNo chosen)
         'tDisplay'    : 'yes',   # always yes
         'Page'        : '1',     # number undetermined, should be updated after first query
         'UserProfile' : '0',     # always 0
         'LanguageId'  : '1'  }   # 1:Arabic, 2:English

USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/44.0.2403.89 Chrome/44.0.2403.89 Safari/537.36'
headers = {'User-Agent' : USER_AGENT}

# ────────────────────────────────────────────────────
# functions
# ────────────────────────────────────────────────────

def addData2Url(url, data):
    '''Add variables to url and return string'''
    datastr='&'.join(['%s=%s' % (k,v) for k,v in  data.items()])
    return '%s?%s' % (url,datastr)

def combinations(refs):
    '''Generates all combinations of variables for URL requests and return tuple of strings.'''
    for madhn,listtafsir in madhabTafsirTable.items():
        for tafsirn in listtafsir:
            for soran,ayan in refs:
                yield str(madhn),str(tafsirn),soran,ayan


#def requestURL(url, data, debug):
#    '''Request URL and return html string.'''
#    fullurl = addData2Url(url, data)
#
#    display = Display(visible=0, size=(1024, 768)) 
#    
#    display.start() 
#    driver = webdriver.Firefox()
#
#    driver.implicitly_wait(3)
#    driver.set_page_load_timeout(10) #FIXME
#
#    driver.get(fullurl)
#
#    if debug:
#        print('**', fullurl, file=stderr) #DEBUG
#
#    soup = BeautifulSoup(driver.page_source, 'html.parser')
#    driver.quit()
#
#    return soup

def requestURL(url, data, debug):
    '''Request URL and return html string.'''
    fullurl = addData2Url(url, data)

    opts = Options()
    opts.headless = True
    assert opts.headless

    driver = webdriver.Firefox(options=opts)
    driver.implicitly_wait(3)
    driver.set_page_load_timeout(10) #FIXME

    waiting = True
    while waiting:
        try:
            driver.get(fullurl) # TimeoutException: Message: Timeout loading page after 300000ms
            waiting = False
        except TimeoutException:
            driver.quit() #FIXME

            driver = webdriver.Firefox(options=opts) #FIXME
            driver.implicitly_wait(3)
            driver.set_page_load_timeout(10) #FIXME

    if debug:
        print('**', fullurl, file=stderr) #DEBUG

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    driver.quit()

    return soup

#def requestURL(url,data):
#    '''Request URL and return html string.'''
#    fullurl = addData2Url(url,data)
#    print('>>', fullurl, file=stderr) #DEBUG
#    req = Request(fullurl, headers=headers)
#    resp = urlopen(req)
#    return BeautifulSoup(resp.read(), features="lxml")


#DEPRECATED unexpected behaviour using the dictionary in a POST request, better to use a GET request (above)
#           all requests always yiled Page=1 regarless how we have set this variable
#def requestURL():
#    info = parse.urlencode(VARS)
#    info = info.encode('utf-8') # encode data in bytes
#    req = Request(URL, info, headers=headers)
#    resp = urlopen(req)
#    return BeautifulSoup(resp.read())

def saveRetrivedURL(soup, madhn, tafsirn, soran, ayan, pagen, debug):
    '''Saves html string into a file.'''
    # check query is not empty
    #if soup.body.div.find('div', id='SearchResults'): #FIXME

    if soup.find('div', class_='TextArabic'):
        fname = 'altafsir-%s-%s-%s-%s-%s.html' % (madhn, tafsirn, soran, ayan, pagen)
        fpath = joinpath(args.directory_name, fname)
        print('>>', fpath, file=stderr) #DEBUG
        with open(fpath,'w') as outf:
            print(soup.prettify(), file=outf)
    return

# ────────────────────────────────────────────────────
# main
# ────────────────────────────────────────────────────

# load references
if args.ref_file:
    refs = loadReferences(args.ref_file)
else:
    refs = set(generateSuraAya())


files_ = set(tuple(map(int, f.name.rsplit('.', 1)[0].split('-')[1:-1])) for f in os.scandir('../../data/all/ori') if f.is_file())

files_done = []
for i in sorted(files_):
    if (i[0], i[1]) == (2, 26): #FIXME
        files_done.append(i)

try:
    files_done.pop(-1)
except:
    pass


for madhn, tafsirn, soran, ayan in combinations(sorted(refs)):

    if not (int(madhn)==2 and int(tafsirn)==26): continue #NOTE

    #if int(madhn)==1 and int(tafsirn)==1 and soran<54: continue #FIXME
    #if int(madhn)==1 and int(tafsirn)==1 and soran==54 and ayan<43: continue #FIXME

    # skip files already done
    if (int(madhn), int(tafsirn), soran, ayan) in files_done:
        #print('done!', (int(madhn), int(tafsirn), soran, ayan), file=stderr)
        continue


    VARS['tMadhNo']   = madhn
    VARS['tTafsirNo'] = tafsirn
    VARS['tSoraNo']   = soran
    VARS['tAyahNo']   = ayan
    VARS['Page']      = '1'

    # open and read url
    respData = requestURL(URL, VARS, args.debug)

    # save html of query for page 1
    saveRetrivedURL(respData, madhn, tafsirn, soran, ayan, '1', args.debug)
    
    #sleep(uniform(SLEEP_MIN, SLEEP_MAX))  # wait random time to do each query

    # check if query has more than one page
    pages = respData.find_all('a', {'href' : lambda x: x and x.startswith('Javascript:InnerLink_onchange')})

    if pages:
        npages = max(set(int(tag['href'].split(',')[-2]) for tag in pages))

        # skip first page, already retrieved
        currentpage=2

        while currentpage <= npages:

            # update page to retrieve new query
            VARS['Page'] = str(currentpage)

            # open and read url
            respData = requestURL(URL, VARS, args.debug)

            # save html of query for page 1
            saveRetrivedURL(respData, madhn, tafsirn, soran, ayan, currentpage, args.debug)

            #sleep(uniform(SLEEP_MIN, SLEEP_MAX))

            # update npages in case there are more than expected in page 1 query
            # eg: query 1-1-2-233 indicates that there are: 1..10 مزيد pages, the total is 22, so npage should be updated twice
            pages = respData.find_all('a', {'href' : lambda x: x and x.startswith('Javascript:InnerLink_onchange')})
            if pages:
                npages = max(set(int(tag['href'].split(',')[-2]) for tag in pages))

            currentpage+=1
