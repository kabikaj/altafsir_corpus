#!/usr/bin/env python3
#
#     checker_crawler.py
#
# check there are not madhab,tafsir pairs missing
#
# dependencies:
#     util.py
# 
# usage:
#   $ python checker_crawler.py ../../data/all/ori
#
#############################################################################

import os
import sys
from argparse import ArgumentParser,FileType

import util

if __name__ == '__main__':

    parser = ArgumentParser(description='check there are not madhab,tafsir pairs missing')
    parser.add_argument('dir', action='store', help='directory to check')
    args = parser.parse_args()

    verses = list(util.generateSuraAya())
    
    indexes = []
    for madhab, tafasir in util.madhabTafsirTable.items():
        for tafsir in tafasir:
            for sura, aya in verses:
                indexes.append((madhab, tafsir, sura, aya))

    indexes = set(indexes)

    files = set(tuple(map(int, f.name.rsplit('.', 1)[0].split('-')[1:-1])) for f in os.scandir(args.dir) if f.is_file())

    # show missing files
    for index in indexes:
        if index not in files:
            print(index)

        