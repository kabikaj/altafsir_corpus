#!/usr/bin/env python
#
#     offserbuilder.py
#
# Input format:
#
#     { "ayahtexts" : [ "verse1" ,
#                       "verse2" ,
#                       ...
#       ] ,
#       "commentaries" : [ { "type" : plain|ayah|hadith|verse ,
#                            "text" : text1
#                          } ,
#                          ...
#       ]
#     }
#
# Output format:
# 
#     { "text" : "alltexts" ,
#       "annotation" : { "aya" : [ { "start" : int, "end" : int } ,
#                                  { "start" : int, "end" : int } ,
#                                  ...
#                        ] ,
#                        "hadith" : [
#                                  ...
#                        ] ,
#                        "verse" : [
#                                  ...
#                        ]
#       }
#     }
#
# usage:
#   $ python offsetbuilder.py ../../data/all/merged ../../data/all/final
#
##########################################################################

import os
import json
from argparse import ArgumentParser, FileType


if __name__ == '__main__':

    parser = ArgumentParser(description='Collects all texts from each file and create offset annotations')
    parser.add_argument('input_dir', help='Input directory with json formats')
    parser.add_argument('output_dir', help='Output directory to store new json files')
    args = parser.parse_args()

    # get all json files from input_dir
    fnames = ((f.name, f.path) for f in os.scandir(args.input_dir) if f.is_file() and os.path.splitext(f.name)[1]=='.json')

    for fbase, fpath in fnames:

        with open(fpath) as fp:
            fobj = json.load(fp)

        commentaries = fobj['commentaries']

        alltext = ''
        annotation = {'aya' : [], 'hadith' : [], 'verse' : []}
        pivot = 0   # current position

        for com in commentaries:

            ann = com['type']
            text = com['text']

            alltext += ' %s' % text

            if ann == 'ayah':

                annotation['aya'].append({'start' : pivot, 'end' : pivot + len(text)})
            
            elif ann == 'hadith':
                annotation['hadith'].append({'start' : pivot, 'end' : pivot + len(text)})

            elif ann == 'verse':
                annotation['verse'].append({'start' : pivot, 'end' : pivot + len(text)})

            pivot = len(alltext)


        with open(os.path.join(args.output_dir, fbase), 'w') as outfp:

            json.dump({'text' : alltext, 'annotation' : annotation}, outfp, ensure_ascii=False)

