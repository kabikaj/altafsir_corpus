
# Altafsir Corpus

> Corpus of Quranic exegesis

Altafsir corpus was extracted from altafsir.com and organised in json files. Each file contains the text along with different types of annotations. The annotations are offsets that point to the texts. After scraping and restructuring the corpus, all texts has been tokenised, tagged with POS and lemmatised using the CAMeLTools package.

A copy of the texts of the corpus have also been placed in the OpenITI Repository.


## Description of the corpus extraction

Scripts and relevant info for downloading all quranic commentaries from [altafsir.com][altafsir] concerning specific *sura* and *ayah* references. List of files:

[altafsir]: http://www.altafsir.com

* `crawler.py` - Script that downloads all html files corresponding to *sura:ayah* references given a reference file to given output directory.

* `scraper.py` - Script that extracts relevant information from original html files and dumps it into json files according to each different query.

* `merger.py` - Script that removes files with duplicate contain and fix inconsistencies in quranic indexes.

* `offsetbuilder.py` - Script that joins all text within each file and creates offsets for annotation.

* `selector.py` - Script that selects the relevant files for COBHUNI project.

* `util.py` - Utility functions and tables containing sura:ayah and madhab:tafsir correspondences.

* **references.txt** - *Sura:ayah* references.

* **madhab-names.txt** - Index and madhab name correspondences.

* **tafsir-names.txt** - Index and tafsir name, author and date correspondences.


## Author

Alicia González Martínez as part of the ERC-Project COBHUNI 